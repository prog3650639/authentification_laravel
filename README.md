# Apropos

C'est un projet de petite envergure sur Laravel où nous avons le besoin d'utiliser une table différente de "users" pour l'authentification.

# 1ere étape :

Nous allons illustrer cela en utilisant la table "etudiant" comme exemple. 

Creer une modele avec une migration pour la table étudiant : 

    php artisan make:model etudiant -m

# 2eme étape :

définir les colonnes de la table étudiant dans la migration (database/migrations/.....create_etudiant)

    public function up(): void
    {
        Schema::create('etudiant', function (Blueprint $table) {
            $table->string('id')->default(DB::raw("'etudiant' || nextval('etudiant_sequence')::TEXT"))->primary();
            $table->string('nom_etudiant');
            $table->integer('profil');
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

# 3ème étape : 

aller dans le modèle etudiant et définir ces attributs  :  

        <?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;


    class etudiant extends Authenticatable
    {
    use HasFactory, Notifiable;
    protected $table = 'etudiant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nom_etudiant',
        'profil',
        'email',
        'password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
    }

# 4ème étape : 
définir un guard dans le modèle  : 

    protected $guard = 'etudiantGuard';

# 5ème étape  : 

réglage du guard : 
dans config/auth.php 

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
        'etudiantGuard' => [
            'driver' => 'session',
            'provider' => 'etudiant',
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => env('AUTH_MODEL', App\Models\User::class),
        ],
        'etudiant' => [
            'driver' => 'eloquent',
            'model' => env('AUTH_MODEL', App\Models\etudiant::class),
        ],


        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => env('AUTH_PASSWORD_RESET_TOKEN_TABLE', 'password_reset_tokens'),
            'expire' => 60,
            'throttle' => 60,
        ],
        'etudiant' => [
            'provider' => 'etudiant',
            'table' => env('AUTH_PASSWORD_RESET_TOKEN_TABLE', 'password_reset_tokens'),
            'expire' => 60,
            'throttle' => 60,
        ],
    ],


