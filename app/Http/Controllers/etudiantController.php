<?php

namespace App\Http\Controllers;

use App\Http\Requests\loginRequest;
use App\Models\etudiant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class etudiantController extends Controller
{
    public function createEtudiant(){
        etudiant::create([
            'nom_etudiant'=>'thony',
            'profil'=>1,
            'email'=>'thony@gmail.com',
            'password'=>Hash::make('0000')
        ]);
        return view('authentification.login');
    }

    public function login(){
        return view('authentification.login');
    }

    public function doLogin(loginRequest $request){
        $input_values = $request->validated();
        dd(Auth::guard('etudiantGuard')->attempt($input_values));
    }
}
