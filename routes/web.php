<?php

use App\Http\Controllers\etudiantController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('authentification.login');
});


Route::get('/createEtudiant', [etudiantController::class, 'createEtudiant']);


Route::get('/login', [etudiantController::class, 'login'])->name('auth.login');
Route::post('/login', [etudiantController::class, 'doLogin']);

